import { Deserializable } from "../models/deserializable.model";

export class Title implements Deserializable {
  public id: number;
  public name: string;
  public deleted: number;

  constructor() {

  }
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
