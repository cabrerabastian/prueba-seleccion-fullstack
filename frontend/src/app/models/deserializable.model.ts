/** Assign to models */
export interface Deserializable {
  deserialize(input: any): this;
}