import { Deserializable } from "../models/deserializable.model";
import { Book } from "../models/book.model";
import { Title } from "../models/title.model";

export class Character implements Deserializable {
  public id: number;
  public _id: string;
  public male: number;
  public house: string;
  public slug: string;
  public name: string;
  public imageLink: string;
  public createdAt: Date;
  public updatedAt: Date;
  public __v: number;
  public titles: Title[];
  public books: Book[];
  public deleted: number;


  constructor() {

  }
  deserialize(input: any) {
    Object.assign(this, input);
    //Assign titles to Character
    this.titles = this.titles ? this.titles.map(title => {
      return new Title().deserialize(title);
    }) : [];
    //Assign Books to Character
    this.books = this.books ? this.books.map(book => {
      return new Book().deserialize(book);
    }) : [];
    return this;
  }
  getAllBooks() {
    const bookNames = [];
    this.books.forEach(book => {
      bookNames.push(book.name);
    })
    return bookNames.length ? bookNames.join(',') : 'No tiene libros asociados';
  }
  getAllTitles() {
    const titleNames = [];
    this.titles.forEach(title => {
      titleNames.push(title.name);
    })
    return titleNames.length ? titleNames.join(',') : 'No tiene titulos asociados';
  }
  //check for http image
  checkImage() {
    return this.imageLink ? this.imageLink.includes('http') : false;
  }
}
