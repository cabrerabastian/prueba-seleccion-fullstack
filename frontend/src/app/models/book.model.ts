import { Deserializable } from "../models/deserializable.model";


export class Book implements Deserializable {
  public id: number;
  public name: string;
  public deleted: number;

  constructor() {

  }
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
