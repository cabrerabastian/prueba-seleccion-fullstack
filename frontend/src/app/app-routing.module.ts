import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


//routes
const appRoutes: Routes = [
  { path: 'list', loadChildren: './main/characters/list/list.module#ListModule' },
  { path: 'view', loadChildren: './main/characters/view/view.module#ViewModule' },
  {
    path: '**',
    redirectTo: 'list'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
