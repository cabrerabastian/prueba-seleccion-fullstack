import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as ENV } from './../../environments/environment';
import { Character } from './../models/character.model';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  apiUrl: string = ENV.BASE_URL;
  constructor(public http: HttpClient) { }

  //get all characters
  public getAll() {
    return new Promise(resolve => {
      this.http.get(`${this.apiUrl}/characters`).subscribe((res: any) => {
        //Pass to Object
        res.characters = res.characters.map(character => {
          return new Character().deserialize(character);
        })
        resolve(res);
      }, err => {
        resolve(err);
      });
    });
  }
  //get character by id
  public getById(characterId: number) {
    return new Promise(resolve => {
      this.http.get(`${this.apiUrl}/characters/${characterId}`).subscribe((res: any) => {
        //Pass to Object
        res.character = new Character().deserialize(res.character);
        resolve(res);
      }, err => {
        resolve(err);
      });
    });
  }
}
