import { Component, OnInit } from '@angular/core';
import { CharacterService } from 'src/app/services/character.service';
import { Character } from 'src/app/models/character.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  characters: Character[] = []
  tempCharacters: Character[] = []
  filterCharacters: Character[] = [];
  errorMessage: string = '';
  query: string = '';
  maxCharacter: number = 9;
  actualPage: number = 0;
  prevPage: number = 0;
  nextPage: number = 1;
  paginateWithSearch: number = null;
  blockNexPage: boolean = false;
  constructor(private characterService: CharacterService, private router: Router) {
    this.getAllCharacter();
  }

  ngOnInit() {
  }
  private getAllCharacter() {
    this.characterService.getAll().then((data: any) => {
      if (data.error) {
        this.errorMessage = data.error.message;
      } else {
        this.characters = data.characters;
        this.tempCharacters = this.characters;
        this.errorMessage = '';
        this.paginateList(0);
      }
    });
  }
  updateList(event) {

    let val = event.target.value.toLowerCase();;
    // filter our data
    const temp = this.tempCharacters.filter((character) => {
      return (character.name && character.name.toLowerCase().indexOf(val) !== -1) ||
        (character.house && character.house.toString().toLowerCase().indexOf(val) !== -1) ||
        !val;
    });

    // update the rows
    this.characters = temp;
    this.filterCharacters = this.characters;
    this.paginateWithSearch = val ? 1 : null;
    this.paginateList(0, 1);
  }
  paginateList(pageNumber: number, type: number = null) {
    this.actualPage = pageNumber;
    const toFilter = type ? this.filterCharacters : this.tempCharacters;
    this.characters = toFilter.filter((character, idx) => idx >= pageNumber * this.maxCharacter && idx <= (pageNumber + 1) * this.maxCharacter);
    this.checkForNextButton();
  }
  checkForNextButton() {
    if (this.characters.length < 10) {
      this.blockNexPage = true;
    } else {
      this.blockNexPage = false;
    }
  }
  openView(id: number) {
    this.router.navigate([`view/${id}`]);
  }
}
