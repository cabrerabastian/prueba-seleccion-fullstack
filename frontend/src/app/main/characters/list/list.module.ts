import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { RouterModule } from '@angular/router';
import { CharacterService } from 'src/app/services/character.service';

const routes = [
  {
    path: '',
    component: ListComponent
  }
];
@NgModule({
  declarations: [ListComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  providers: [
    CharacterService
  ]
})
export class ListModule { }
