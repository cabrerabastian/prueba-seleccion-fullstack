import { Component, OnInit, OnDestroy } from '@angular/core';
import { Character } from 'src/app/models/character.model';
import { CharacterService } from 'src/app/services/character.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit, OnDestroy {
  character: Character;
  characterId: number;
  errorMessage: string = '';
  routeSubscription: Subscription;
  constructor(private characterService: CharacterService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    // get id
    this.routeSubscription = this.route.params.subscribe((param: any) => {
      this.characterId = param.characterId;
      this.getById();
  
    });
  }
  private getById() {
    this.characterService.getById(this.characterId).then((data: any) => {
      if (data.error) {
        this.errorMessage = data.error.message;
      } else {
        this.errorMessage = '';
        this.character = data.character;
      }
    });
  }
  ngOnDestroy() {
    //destroy subscription get id
    this.routeSubscription.unsubscribe();
  }
}
