# Test Prueba Selección FullStack - Front

Este repositorio se realizó en Angular 8.

## Nota importante
Los datos que se basaron la construccion de la base de datos, back y front estan basados en /api/map/characters de la página de GOT.

## Instalación

```bash
npm install
```
## URL de consumo de API
La URL de consumo de API se puede cambiar en src/app/environments

## Iniciar sistema

```bash
ng serve --open
```