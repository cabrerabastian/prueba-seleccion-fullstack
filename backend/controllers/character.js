
const Character = require('../models').character;
const Book = require('../models').book;
const Title = require('../models').title;
const models = require('../models');

const getAll = (req, res) => {
  return Character.findAll({
    where: {
      deleted: 0,
    },
    include: [
      {
        model: Book,
        where: {
          deleted: 0
        },
        required: false
      },
      {
        model: Title,
        where: {
          deleted: 0
        },
        required: false
      },
    ]
  })
    .then(characters => {
      return res.status(200).json({
        error: false,
        characters
      })
    })
    .catch(error => {
      return res.status(400).json({
        error: true,
        message: `Problema con obtener personajes de GOT ${error}`
      })
    });
}
const getById = (req, res) => {
  const characterId = req.params.id;

  return Character.findOne({
    where: {
      deleted: 0,
      [models.sequelize.Op.or]: [{ id: characterId }, { _id: characterId }]
    },
    include: [
      {
        model: Book,
        where: {
          deleted: 0
        },
        required: false
      },
      {
        model: Title,
        where: {
          deleted: 0
        },
        required: false
      },
    ]
  })
    .then(character => {
      if (!character) {
        return res.status(404).json({
          error: true,
          message: `Personaje no encontrado cuya id es ${characterId}`
        })
      }
      return res.status(200).json({
        error: false,
        character
      })
    })
    .catch(error => {
      return res.status(400).json({
        error: true,
        message: `Problema con obtener personajes de GOT ${error}`
      })
    });
}
module.exports = {
  getAll,
  getById
};