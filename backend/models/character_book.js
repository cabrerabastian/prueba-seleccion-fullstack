/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('character_book', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    character_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'character',
        key: 'id'
      }
    },
    book_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'book',
        key: 'id'
      }
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'character_book',
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
};
