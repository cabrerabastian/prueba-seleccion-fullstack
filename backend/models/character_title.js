/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('character_title', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    character_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'character',
        key: 'id'
      }
    },
    title_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'title',
        key: 'id'
      }
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'character_title',
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
};
