/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  const Character = sequelize.define('character', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    _id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    male: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    house: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    slug: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    imageLink: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    __v: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    deleted: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: 0
    }
  }, {
    tableName: 'character',
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
  //associate
  Character.associate = models => {
    Character.belongsToMany(models.book, { through: 'character_book' }),
    Character.belongsToMany(models.title, { through: 'character_title' })
  };
  return Character;
};
