var express = require('express');
var router = express.Router();
const characterController = require('../controllers').characterController;

/** GET */
router.get('/', characterController.getAll);
router.get('/:id', characterController.getById);

module.exports = router;