# Test Prueba Selección FullStack - Back

Este repositorio se realizó en Node utilizando Express y Sequelize. La base de datos utilizadas fue MySQL.

## Nota importante
Los datos que se basaron la construccion de la base de datos, back y front estan basados en /api/map/characters de la página de GOT.

## Instalación

```bash
npm install
```
## Base de datos
Dentro del proyecto existe una carpeta llamada "bd", en la cual se encuentra el archivo .sql ("test-fullstack.sql") utilizado en este proyecto. Es necesario cargar este archivo en su servidor local de MySQL.

Si no desea utilizar el archivo .sql, la estructura es la siguiente
```bash

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for character
-- ----------------------------
DROP TABLE IF EXISTS `character`;
CREATE TABLE `character`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_id` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Id from API https://api.got.show/',
  `male` tinyint(1) NULL DEFAULT NULL,
  `house` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imageLink` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createdAt` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `__v` tinyint(1) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_id_web`(`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for character_book
-- ----------------------------
DROP TABLE IF EXISTS `character_book`;
CREATE TABLE `character_book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `character_book_ibfk_1`(`character_id`) USING BTREE,
  INDEX `character_book_ibfk_2`(`book_id`) USING BTREE,
  CONSTRAINT `character_book_ibfk_1` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `character_book_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for character_title
-- ----------------------------
DROP TABLE IF EXISTS `character_title`;
CREATE TABLE `character_title`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `character_title_ibfk_1`(`character_id`) USING BTREE,
  INDEX `title_id`(`title_id`) USING BTREE,
  CONSTRAINT `character_title_ibfk_1` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `character_title_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `title` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for title
-- ----------------------------
DROP TABLE IF EXISTS `title`;
CREATE TABLE `title`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

```

Si desea cambiar la configuración de la base de datos del proyecto, esta se puede realizar en el archivo "config/config.json"

## Carga masiva de datos
Para iniciar la carga masiva de datos de la página https://api.got.show es necesario ejecutar el siguiente comando

```bash
node collect.js
```
Este comando poblará la base de datos con los personajes de GOT obtenidos.
## Iniciar sistema

```bash
npm run start-dev
```

## API
Obtener todos los personajes
```bash
http://localhost:3000/characters/
```

Obtener personaje por id (el sistema busca tanto por id de MySQL como por _id obtenida de la web de GOT)
```bash
http://localhost:3000/characters/:id
```