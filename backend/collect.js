
const config = require('./config');
const axios = require("axios");
const Character = require('./models').character;
const Book = require('./models').book;
const Title = require('./models').title;
const CharacterTitle = require('./models').character_title;
const CharacterBook = require('./models').character_book;
const models = require('./models');

const urlGOT = config.URL_GOT;
axios.get(urlGOT).then(async (responseGOT) => {
  //get data from response
  const iterationCharacterPromise = models.sequelize.Promise.each(responseGOT.data.data, async character => {
    const books = character.books;
    const titles = character.titles;
    // ids of books to add relation with character
    const booksToAdd = [];
    // ids of titles to add relation with character
    const titlesToAdd = [];
    const addCharacterPromise = Character.findOne({
      where: {
        _id: character._id
      },
    })
      .then(characterDB => {
        //if not exists added, if exists with deleted 1, update
        if (!characterDB) {
          //Add character
          return Character
            .create(character)
            .then(characterCreated => {
              console.log(`Personaje creado: ${characterCreated.name}`);
              //Return only character id
              return characterCreated.dataValues.id;
            })
            .catch(error => {
              console.log(`Problemas con agregar personaje ${characterCreated.name} ${error}`);
              process.exit();
            });

        } else if (characterDB.dataValues.deleted) {
          //Update data
          return characterDB
            .update({
              deleted: 0,
              updatedAt: new Date()
            })
            .then(characterUpdated => {
              console.log(`Personaje Actualizado: ${characterDB.name}`);
            })
            .catch(error => {
              console.log(`Problemas con actualizar personaje ${characterDB.name} ${error}`);
              process.exit();
            });
        }
      })
      .catch(error => {
        console.log(`Problemas con obtener los datos del personaje ${error}`);
        process.exit();
      });

    // Search for books in DB
    const iterationBookPromise = models.sequelize.Promise.each(books, async book => {
      const bookCreatePromise = Book.findOne({
        where: {
          name: book
        },
      })
        .then(bookDB => {
          // Not exists
          if (!bookDB) {
            //Add book
            return Book
              .create({
                name: book
              })
              .then(bookCreated => {
                console.log(`Libro agregado: ${book}`);
                // add books ids
                booksToAdd.push(bookCreated.dataValues.id);
              })
              .catch(error => {
                console.log(`Problemas con agregar libro ${book} ${error}`);
                process.exit();
              })
          } else {
            //if exists
            //add books ids
            booksToAdd.push(bookDB.dataValues.id);
          }
        })
        .catch(error => {
          console.log(`Problemas con encontrar libro ${book} ${error}`);
          process.exit();
        });
      await Promise.resolve(bookCreatePromise);
    });

    // Search for titles in DB
    const iterationTitles = models.sequelize.Promise.each(titles, async title => {
      const titleCreatePromise = Title.findOne({
        where: {
          name: title
        },
      })
        .then(titleDB => {
          // Not exists
          if (!titleDB) {
            //Add title
            return Title
              .create({
                name: title
              })
              .then(titleCreated => {
                console.log(`Título agregado: ${title}`);
                // add books ids
                titlesToAdd.push(titleCreated.dataValues.id);
              })
              .catch(error => {
                console.log(`Problemas con agregar título ${title} ${error}`);
                process.exit();
              })
          } else {
            //if exists
            // add title to ids
            titlesToAdd.push(titleDB.dataValues.id);
          }
        })
        .catch(error => {
          console.log(`Problemas con encontrar título ${title} ${error}`);
          process.exit();
        });
      await Promise.resolve(titleCreatePromise);
    });

    await Promise.all([addCharacterPromise, iterationBookPromise, iterationTitles]).then((promiseValues) => {
      //Create Bulk data to optimize the query
      const bookCharacterBulk = booksToAdd.map(bookId => ({ book_id: bookId, character_id: promiseValues[0] }));
      const titleCharacterBulk = titlesToAdd.map(titleId => ({ title_id: titleId, character_id: promiseValues[0] }));

      //Add Character Books
      CharacterBook
        .bulkCreate(bookCharacterBulk)
        .then(bookCharacterCreated => {
          console.log(`Libros agregados correctamente`);
          //Add Character Titles
          CharacterTitle
            .bulkCreate(titleCharacterBulk)
            .then(titleCharacterCreated => {
              console.log(`Titulos agregados correctamente`);
            })
            .catch(error => {
              console.log(`Problemas con agregar titulos ${error}`);
              process.exit();
            });
        })
        .catch(error => {
          console.log(`Problemas con agregar libros ${error}`);
          process.exit();
        });
    });
  });
  await Promise.resolve(iterationCharacterPromise).then(() => {
    console.log(`* Información almacenada correctamente *`);
    process.exit();
  });
}).catch((error) => {
  // handle error
  console.log(`Problemas con obtener la lista de personajes de GOT ${error}`);
  process.exit();
});
