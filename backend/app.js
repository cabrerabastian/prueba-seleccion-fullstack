const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');

const indexRouter = require('./routes/index');
const characterRouter = require('./routes/characters');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.all('*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});
const livereload = require('easy-livereload');
const file_type_map = {
  jade: 'html', // `index.jade` maps to `index.html`
  styl: 'css', // `styles/site.styl` maps to `styles/site.css`
  scss: 'css', // `styles/site.scss` maps to `styles/site.css`
  sass: 'css', // `styles/site.scss` maps to `styles/site.css`
  less: 'css', // `styles/site.scss` maps to `styles/site.css`,
  js: 'js'
  // add the file type being edited and what you want it to be mapped to.
};

// store the generated regex of the object keys
const file_type_regex = new RegExp('\\.(' + Object.keys(file_type_map).join('|') + ')$');

app.use(livereload({
  watchDirs: [
    path.join(__dirname, 'public'),
    path.join(__dirname, 'app'),
    path.join(__dirname, 'controllers')
  ],
  checkFunc: function (file) {
    return file_type_regex.test(file);
  },
  renameFunc: function (file) {
    // remap extention of the file path to one of the extentions in `file_type_map`
    return file.replace(file_type_regex, function (extention) {
      return '.' + file_type_map[extention.slice(1)];
    });
  },
  port: process.env.LIVERELOAD_PORT || 35729
}));

/** Routers */
app.use('/', indexRouter);
app.use('/characters', characterRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
